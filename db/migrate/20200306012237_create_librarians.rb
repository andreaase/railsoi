class CreateLibrarians < ActiveRecord::Migration[5.1]
  def change
    create_table :librarians do |t|
      t.string :name

      t.timestamps
    end
  end
end
